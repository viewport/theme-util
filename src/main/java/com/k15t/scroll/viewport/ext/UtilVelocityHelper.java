package com.k15t.scroll.viewport.ext;


import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;

import java.util.List;


public class UtilVelocityHelper {

    private final SpaceManager spaceManager;


    public UtilVelocityHelper(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }


    public List<Space> getAllSpaces() {
        return spaceManager.getAllSpaces();
    }

}
